// exercice 1

for(let i = 0; i <= 8; i++){
    console.log(`table de ${i}\n`)
    for(let y = 0; y < 10; y++){
        console.log(`${i} x ${y} = ${i*y}\n`)
    }
    console.log(`\n`)
}


// exercice 2

const body = document.body

const ul = document.createElement("ul")

for(let i = 0; i <= 10; i++){
    let li = document.createElement("li")
    let text = document.createTextNode(`${i} x 5 = ${i*5}`) 
    
    li.appendChild(text)
    ul.appendChild(li)
}

body.appendChild(ul)

// exercice 3
let i = 0 

while ( true ){
    // safe while true
    if(i === Number.MAX_SAFE_INTEGER)
        return

    console.log(`${i} x 5 = ${i*5}`)
    i++;
}

// exercice 4

const tab = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

tab.map((value)=>{
    console.log(`${value} x 5 = ${value*5}`)
    return value
})

// exercice 5
const obj = {a: "42", b: "42" }
let result = 1

Object.keys(obj).forEach(value => {
    result *= Number(value)
})

console.log(result)

// exercice 6
const tab2 = [5, 4, 3, 2, 1]

for(let i = 0; i < tab2.length - 1; i++){
    for(let y = 0; y < tab2.length - 1; y++){
        if(tab2[y] > tab2[y+1]){
            let swap = tab2[y]
            tab2[y] = tab2[y+1]
            tab2[y+1] = swap 
        }
    }
}

console.log(tab2)

// exercice 7
const tabProduits = []

for(let i = 0; i < 10 - 1; i++){
    tabProduits.push({ 
        nom: `produits ${i}`,
        prix: 10*i+1, 
        description: `voicie la description du produit`,
        image: "https://img-0.journaldunet.com/JgOAEEaKp00acGdrktPUB8Y2__8=/1500x/smart/32d90de13a5f411c86709152f70fc67c/ccmcms-jdn/10861192.jpg",
        lien: "https://boutique.orange.fr/internet-mobile/pack-internet-mobile-fibre?offreInternet=livebox-fibre&offreMobile=forfait-100go-5g-sim-open"
    })
}

tabProduits.forEach(produit => {
    let div = document.createElement("div")

    let h2 = document.createElement("h2")
    h2.appendChild(document.createTextNode(produit.nom))
    div.appendChild(h2)

    let pPris = document.createElement("p")
    pPris.appendChild(document.createTextNode(produit.prix))
    div.appendChild(pPris)

    let pDerscriptio = document.createElement("p")
    pDerscriptio.appendChild(document.createTextNode(produit.description))
    div.appendChild(pDerscriptio)

    let img = document.createElement("img")
    img.setAttribute("src", produit.image)
    div.appendChild(img)

    let a = document.createElement("a")
    a.setAttribute("herf", produit.lien)
    a.appendChild(document.createTextNode("voir fiche"))
    div.appendChild(a)
    
    document.body.appendChild(div)
})

// exercice 8

const d = new Date();

const select = document.createElement("select")
for(let i = 1980; i <= d.getFullYear(), i++;){
    let option = document.createElement("option");

    option.appendChild(document.createTextNode(i))
    select.appendChild(option)
}   

document.body.appendChild(select);

// exercice 9
const input = document.createElement("input")
let text = ""

input.addEventListener('input', (e) => {
    text = e.target.value
})

const resultTab = tabProduits.filter((value) => {
    if(text.length > 3)
        return
    if(text === value.nom)
        return value
})

resultTab.forEach(produit => {
    let div = document.createElement("div")

    let h2 = document.createElement("h2")
    h2.appendChild(document.createTextNode(produit.nom))
    div.appendChild(h2)

    let pPris = document.createElement("p")
    pPris.appendChild(document.createTextNode(produit.prix))
    div.appendChild(pPris)

    let pDerscriptio = document.createElement("p")
    pDerscriptio.appendChild(document.createTextNode(produit.description))
    div.appendChild(pDerscriptio)

    let img = document.createElement("img")
    img.setAttribute("src", produit.image)
    div.appendChild(img)

    let a = document.createElement("a")
    a.setAttribute("herf", produit.lien)
    a.appendChild(document.createTextNode("voir fiche"))
    div.appendChild(a)
    
    document.body.appendChild(div)
})

// exercice 10

for(let i = 1; i < 10; ){
    let ligne = ""
 

    for(let y = 1; y < 11; y++){
        ligne += '1'

    }
    console.log(ligne)
}

for(let i = 1; i < 10; i++){
    let ligne = ""
    let nbPrintChar = 0
    let nbBlancCoter = 0
    let nbReprintBlanc = 0 

    for(let y = 1; y < 21; y++){
        nbPrintChar = i * 2 
        nbBlancCoter = (20 - nbPrintChar) / 2
        nbReprintBlanc = (nbBlancCoter + nbPrintChar)

        if(y < nbBlancCoter || y >= nbReprintBlanc){
            ligne += " ";
        }else {
            ligne += "1"
        }

    }
    console.log(ligne)
}
