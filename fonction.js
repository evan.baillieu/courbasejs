// exercice 1
const myPutStr = (str) => {
    if(Number.isInteger(str)){
        return 'est pourquoi pas 42!'
    }

    return str
}

// exercice 2
const computeSurfaceM2 = (a, b) => {
    return a * b
}

// exercice 3

const body = document.body

const entreButton = document.createElement("button")
entreButton.appendChild(document.createTextNode("entre"))
body.appendChild(entreButton)
let text = ""

const createButton = () => {
    const submit = document.createElement("button")
    const input = document.createElement("input")
    const div = document.createElement("div")

    submit.appendChild(document.createTextNode("envoyer"))
    input.addEventListener('input', (e)=>{
        text = e.target.value;
    })

    div.appendChild(input)
    div.appendChild(submit)

    body.removeChild(entreButton)
    body.appendChild(div)
}

entreButton.addEventListener('input', createButton);

// exercice 4

const testTab = [[1, 1, 1, 1, 1], [0, 1, 0, 1, 0], [1, 0, 0, 1, 1]]
const table = document.createElement('table')

const afficheTab = (tab) => {
    tab.forEach(numbers => {
        let tr = document.createElement('tr')
        numbers.forEach((nb) => {
            let td = document.createElement('td');
            td.appendChild(document.createTextNode(`${nb}`))

            tr.appendChild(td);
        })
        table.appendChild(tr)
    });
}

// exercice 5

const date = new Date()
let hour = date.getHours();
let minute = date.getMinutes();
let second = date.getSeconds();

setInterval(()=>{
    if(second < 59){
        second++;
    }else{
        second = 0;
        minute++;
    }

    if(minute === 60){
        minute = 0;
        hour++;
    }

    if(hour === 24){
        hour = 0
    }

    console.log(`${hour}:${minute}:${second}`);
}, 1000)

// exercice 6

const MyElementGenerator = (el, balise, text) => {
    let element = document.createElement(balise)

    if (text){
        element.appendChild(document.createElement(text))
    }

    el.appendChild(element)

    return element
}

// exercice 7
const fibonacie = (index) =>{
    if (index < 0)
	{
		return (-1);
	}
	if (index == 0)
	{
		return (0);
	}
	if (index == 1)
	{
		return (1);
	}

    return fibonacie(index - 1) + fibonacie(index - 2)
}

const sortTabDecroissant = (tab2) => {
    for(let i = 0; i < tab2.length - 1; i++){
        for(let y = 0; y < tab2.length - 1; y++){
            if(tab2[y] < tab2[y+1]){
                let swap = tab2[y]
                tab2[y] = tab2[y+1]
                tab2[y+1] = swap 
            }
        }
    }
}

const calcule = (tabFibonacie) => {
    return tabFibonacie.map(index => {
        return fibonacie(index)
    })
}

const aditionTab = (t) => {
    return t.reduce((currentValue, a) => currentValue + a, 0)
}

(() => {
    const tab = [1,2,3,4,5,6,7,8]
    const tabFibonacie = calcule(tab);
    const sortTab = sortTabDecroissant(tabFibonacie)
    const sumTab = aditionTab(sortTab);
    console.log(sumTab)
})()

console.log(fibonacie(0))