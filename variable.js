'use strict'
// exercice 1

// “My school Forever”.

// execice 2

/*
* ”My school forever every day”
*/

// execice 3

const string = "test"

const  nb = 1

const float = 1.2

const isBol = true

const array = [1, 2, 3, 4]

const date = Date()

const obj = {}

const noDeclare = undefined

const value = null


// exercice 4

let my42count = "quarante-deux".length


// exercice 5
const v = 2

const isSet = v || 42

// exercice 6

const myArray42 = "quarante-deux".split("")

// exercice 7

const myArray42Len = myArray42.length

// exercice 8

const text = `La grande réponse sur la vie, l’univers et le reste !\n${myArray42.join("")}` 

// exercice 9

let rand = Math.floor(Math.random() * 42) + 1;
rand = (rand == 42) || rand

// exercice 10

let obje = typeof {} || typeof []

let number = typeof 3

let strings = typeof "test"

let boolean = typeof true

let nbNull = typeof NaN

let undefine = typeof undefined

let nullable = typeof nullable

// exercice 11 

let compute42 = 42 * 43

compute42 = `${compute42}`

// exercice 12 

let string42 = "42424242".replace(42, "quarante-deux")

// exercice 13 

let a = 24
let b = 42
let c 

c = a
a = b
b = c 